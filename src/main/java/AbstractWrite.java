public abstract class AbstractWrite {

    protected static String pilihan;
    protected Menu menu = new Menu();
    protected String output1 = "data_sekolah_modus.txt";
    protected String output2 = "data_sekolah_modus_median.txt";

    abstract void tulis ();

    void generate(String file){
        menu.menuTop();
        System.out.println("\nFile \"" +file+"\" telah digenerate\ndi direktori C:/temp/direktori\nsilahkan cek.\n");
    }

    void generate(String file1, String file2){
        menu.menuTop();
        System.out.println("\nFile \""+file1+"\" dan \""+file2+"\"\ntelah digenerate\ndi direktori C:/temp/direktori\nsilahkan cek.\n");
    }
}
