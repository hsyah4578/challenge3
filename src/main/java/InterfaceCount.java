import java.io.*;

@FunctionalInterface
public interface InterfaceCount {

    String hitung() throws IOException;

}
