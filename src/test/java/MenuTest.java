import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.Scanner;

public class MenuTest {

    Menu menu = new Menu();

    @Test
    @DisplayName("Tes input menu valid")
    void testmenuAwal (){

        WriteFrekuensi writeFrekuensi = new WriteFrekuensi();
        WriteMean writeMean = new WriteMean();
        menu.menuTop();
        System.out.println("\nLetakkan file csv dengan nama file data_sekolah.csv di direktori\nberikut: C:/temp/direktori\n");
        System.out.println("pilih menu:");
        System.out.println("1. Generate txt untuk menampilkan frekuensi nilai");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median dan modus");
        System.out.println("3. Generate kedua file");
        System.out.println("0. exit");
        System.out.print("\npilihan anda : ");
        String pilihMenu = "1";
        System.out.println();
        AbstractWrite.pilihan = pilihMenu;
        switch (pilihMenu) {
            case "0" :
                //System.out.println("Aplikasi ditutup");
                //System.exit(0);
            case "1" :
                //writeFrekuensi.tulis();
                //menu.back();
                break;
            case "2" :
                //writeMean.tulis();
                break;
            case "3" :
                //writeFrekuensi.tulis();
                //writeMean.tulis();
                break;
            default :
                //menu.inputMenuSalah();
        }
    }

    @Test
    @DisplayName("Tes input menu tidak valid")
    void testmenuAwal2 (){

        WriteFrekuensi writeFrekuensi = new WriteFrekuensi();
        WriteMean writeMean = new WriteMean();
        menu.menuTop();
        System.out.println("\nLetakkan file csv dengan nama file data_sekolah.csv di direktori\nberikut: C:/temp/direktori\n");
        System.out.println("pilih menu:");
        System.out.println("1. Generate txt untuk menampilkan frekuensi nilai");
        System.out.println("2. Generate txt untuk menampilkan nilai rata-rata, median dan modus");
        System.out.println("3. Generate kedua file");
        System.out.println("0. exit");
        System.out.print("\npilihan anda : ");
        String pilihMenu = "a";
        System.out.println();
        AbstractWrite.pilihan = pilihMenu;
        switch (pilihMenu) {
            case "0" :
                //System.out.println("Aplikasi ditutup");
                //System.exit(0);
            case "1" :
                //writeFrekuensi.tulis();
                //menu.back();
                break;
            case "2" :
                //writeMean.tulis();
                break;
            case "3" :
                //writeFrekuensi.tulis();
                //writeMean.tulis();
                break;
            default :
                System.out.println("\nInput yang anda masukkan salah");
        }
    }

    @Test
    @DisplayName("Tes menu back input valid")
    void testback(){
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke Menu utama\n");
        System.out.print("pilihan anda: ");
        String input = "1";
        switch (input){
            case "0" :
                //System.out.println("Aplikasi ditutup");
                //System.exit(0);
            case "1" :
                //menuAwal();
            default:
                //inputMenuSalah();
        }
    }

    @Test
    @DisplayName("Tes menu back input gak valid")
    void testback2(){
        System.out.println("0. Exit");
        System.out.println("1. Kembali ke Menu utama\n");
        System.out.print("pilihan anda: ");
        String input = "z";
        switch (input){
            case "0" :
                //System.out.println("Aplikasi ditutup");
                //System.exit(0);
            case "1" :
                //menuAwal();
            default:
                System.out.println("\nInput yang anda masukkan salah");
        }
    }
}
