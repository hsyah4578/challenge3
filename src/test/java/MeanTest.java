import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import java.io.IOException;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import static org.junit.jupiter.api.Assertions.*;

public class MeanTest {

    private final List<Integer> listNilai = new ArrayList<>();
    private final ReadAndConvertToList convert = new ReadAndConvertToList();

    @Test
    @DisplayName("Test hitung rata-rata")
    void testMean () throws IOException {

        convert.convertToList(listNilai);

        float total = 0;
        for (int t : listNilai){
            total = total + t;
        }
        float hasil = total/ listNilai.size();
        DecimalFormat df = new DecimalFormat("#.##");
        assertEquals("8.32", df.format(hasil));

    }
}
