import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class ReadAndConvertToListTest {

    @Test
    @DisplayName("Tes baca file dan convert ke list sukses -> file ada")
    void testConvertToList () throws IOException {
        List<Integer> list = new ArrayList<>();
        File file = new File("C:\\temp\\direktori\\data_sekolah.csv");
        if (!file.exists()) {
            throw new FileNotFoundException("""

                    File tidak ditemukan.Pastikan anda telah meletakkan file csv
                    dengan nama file data_sekolah.csv di direktori berikut:
                    C:/temp/direktori
                    """);
        }
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String[] tempArr;
        while ((line = br.readLine()) != null) {
            tempArr = line.split(";");
            for (int i = 1; i < tempArr.length; i++) {
                list.add(Integer.parseInt(tempArr[i]));
            }
        }
        br.close();
    }

    @Test
    @DisplayName("Tes baca file dan convert ke list gagal -> file gak ada atau salah path")
    void testConvertToList2 () throws IOException {
        List<Integer> list = new ArrayList<>();
        File file = new File("data_sekolah.csv");
        if (!file.exists()) {
            throw new FileNotFoundException("""

                    File tidak ditemukan.Pastikan anda telah meletakkan file csv
                    dengan nama file data_sekolah.csv di direktori berikut:
                    C:/temp/direktori
                    """);
        }
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        String line;
        String[] tempArr;
        while ((line = br.readLine()) != null) {
            tempArr = line.split(";");
            for (int i = 1; i < tempArr.length; i++) {
                list.add(Integer.parseInt(tempArr[i]));
            }
        }
        br.close();
    }
}
